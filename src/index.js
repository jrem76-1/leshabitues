import React from "react";
import ReactDOM from "react-dom";
import List from './components/List'

const Index = () => {
  return <div>Hello React!</div>;
};

ReactDOM.render(<List />, document.getElementById("index"));