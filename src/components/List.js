import React, { Component } from 'react'
import request from 'superagent'
import styled from 'styled-components'

import ElementList from './ElementList'

const ListStyle = styled.div`
    max-width: 300px;
    margin: 0 auto;
`

export default class List extends Component {
    state = {
        data: null,
        error: false,
    };

    componentDidMount() {

        this.initData();
    }

    initData = () => {
        this.setState({
            data: [],
        })

        request
            .get('https://www.leshabitues.fr/testapi/shops')
            .end((err, res) => {
                if (res) {
                    this.setState({
                        data: res.body.results,
                    })
                }
                if (err) {
                    this.setState({
                        error: true,
                    });
                }
            });
    }

    refreshData = () => {
        this.setState({
            data: [],
        })

        this.initData();
    }

    onClearArray = () => {
        this.setState({
            data: [],
        });
    }

    onReloadData = () => {
        this.initData();
    }

    renderElementList = () => {
        let elementsList = []
        const {
            data
        } = this.state

        if (!data || data.length === 0) {
            return [];
        }

        data.forEach((result, index) => {
            elementsList.push(<ElementList item={result} key={index}></ElementList>)
        });

        return elementsList;
    }

    renderList = (data, error) => {
        if (!data || data === null || data.length === 0) {
            return (
                <ListStyle>Sorry we don't have any result for now.</ListStyle>
            );
        }

        if (error) {
            return (
                <ListStyle>Sorry we're facing some issues for now. Please comeback later</ListStyle>
            );
        }

        return (<ListStyle>
            {this.renderElementList()}
        </ListStyle>);
    }

    render() {

        const {
            data,
            error,
        } = this.state;

        return (<div>
            <button onClick={this.onClearArray}>Reset Array</button>
            <button onClick={this.onReloadData}>Refresh data</button>
            {this.renderList(data, error)}
        </div>)
    }

};