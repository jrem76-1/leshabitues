import React, { Component } from 'react'
import PropTypes from 'prop-types';
import styled from 'styled-components'

const Name = styled.span`
    color: #C4C4C4;
    font-size: 11px;
    text-transform: uppercase;
    letter-spacing: 2px;
`

const Logo = styled.img`
    width: 60px;
    height: 60px;
    margin-right: 10px;
    border: solid 1px #C4C4C4;
`

const ElementListStyle = styled.div`
    display: flex;
    flex-flow: row;
    font-family: Lato;
    color: #C4C4C4;
    font-size: 12px;
    border-bottom: solid 1px #C4C4C4;
    padding: 10px 20px;
`

const AddressStyle = styled.div`
    display: flex;
    flex-direction: column;
`

const RightElement = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`

const MaxOffer = styled.span`
    color: #4A90E2;
`

export default class ElementList extends Component {
    static propTypes = {
        item : PropTypes.object
    }

    render() {
        const {
            item
        } = this.props

        return(<ElementListStyle>
            <div>
                <Logo src={item.logo}></Logo>
            </div>
            <RightElement>
                <Name>{item.name}</Name>
                <AddressStyle>
                    <span>{item.address}</span>
                    <span>{item.zipcode} {item.city}</span>
                </AddressStyle>
                <MaxOffer>up to {item.maxoffer} offered</MaxOffer>
            </RightElement>
        </ElementListStyle>)
    }

};